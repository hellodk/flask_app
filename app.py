from flask import Flask, render_template, request
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def load_home_page():
    return render_template('init.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
